import random

import csv

def create_random_array(lowest, highest, length):
    """
    Returns an array with random items

    """
    random_array = []
    for i in range(length):
        random_item = random.randint(lowest, highest)
        random_array.append(random_item) 
    return random_array

 
def create_reverse_array(lowest, highest, length):
    """
    Returns an array in reverse order
    
    """
    reverse_array = []
    for i in range(length):
        reverse_array.insert(0, i + 1)
    return reverse_array


def create_same_array(number, length):
    """
    Returns an array containing a repeated number

    """
    same_array = []
    for i in range(length):
        same_array.append(number)
    return same_array


def create_sorted_array(length):
    """
    Returns an array in order
    
    """
    sorted_array = []
    for i in range(length):
        sorted_array.append(i + 1)
    return sorted_array


def linear_search(array, item_to_find):
    """
    Returns the amount of searches needed for linear search

    """

    searches = 0
    for item in array:
        searches += 1
        if item == item_to_find:
            return searches
 

def binary_search(sorted_array, item_to_find):
    """
    Returns the amount of searches needed for binary search

    """
    size = len(sorted_array)
    #middle case
    if size == 0:
        return 0
    #middle- found it
    half = size // 2
    if sorted_array[half] == item_to_find:
        return 1
    #searching lower or upper
    if item_to_find < sorted_array[half]:
        #search lower half
        print(sorted_array[:half])
        return 1 + binary_search(sorted_array[:half], item_to_find)
    else:
        #search again in upper half
        print(sorted_array[half+1:])
        return 1 + binary_search(sorted_array[half+1:], item_to_find)


def selection_sort(main_array):
    """
    Returns a sorted array
    """
    array = main_array.copy()
    sorted_array = []
    comparisons = 0
    for i in range(len(array)):
        min_item = array[0]
        element_index = -1
        for item in array:
            element_index += 1
            comparisons += 1
            if min_item > item:
                min_item = item
                min_home = element_index
            if min_item == array[0]:
                min_home = 0
        array.remove(min_item)
        sorted_array.append(min_item)
    return comparisons


def bubble_sort(main_array):
    """
    Returns the amount of comparisons
    """
    array = main_array.copy()
    comparisons = 0
    flag = True
    while flag:
        flag = False
        for i in range(len(array) - 1):
            #if its greater then we swap
            if array[i] > array[i+1]:
                #we swap
                array[i],array[i+1] = array[i+1], array[i]
                flag = True
            comparisons += 1
    return comparisons

#csv files: open, write, close (helpful information)
"""
f = open("test.csv", "w", newline="")
writer = csv.writer(f)
for i in range(18):
    writer.writerow([bubble_sort(create_random_array(1, 10, 10))])
f.close()
"""
random_array = create_random_array(1, 10, 10)
print(random_array)
print(bubble_sort(random_array))
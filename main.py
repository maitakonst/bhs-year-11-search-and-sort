import random

import time

import csv

from utilities import create_random_array

from utilities import create_reverse_array

from utilities import create_same_array

from utilities import create_sorted_array

from utilities import linear_search

from utilities import binary_search

from utilities import selection_sort

from utilities import bubble_sort

 
answer = input("Random, reverse, same or sorted? ")
final_answer = answer.lower()


#csv file actions for sorting

if final_answer == "random":
    sorting_file = open("random.csv", "w", newline="")
elif final_answer == "reverse":
    sorting_file = open("reverse.csv", "w", newline="")
elif final_answer == "same":
    sorting_file = open("same.csv", "w", newline="")
else:
    sorting_file = open("sorted.csv", "w", newline="")
    
writer = csv.writer(sorting_file)

writer.writerow(["Data Points", "Selection", "Bubble"])

for i in range(200):
    i = i + 1
    if final_answer == "random":
        chosen_array = create_random_array(1, i, i)
    elif final_answer == "reverse":
        chosen_array = create_reverse_array(1, i, i)
    elif final_answer == "same":
        chosen_array = create_same_array(1, i)
    else:
        chosen_array = create_sorted_array(i)
    
    selection_comparisons = selection_sort(chosen_array)
    bubble_comparisons = bubble_sort(chosen_array)
    writer.writerow([i, selection_comparisons, bubble_comparisons])

sorting_file.close()


#Sorting time code
"""
SELECTION SORT
    
start_time = time.time()

elapsed_time = time.time() - start_time

print(f"Time: {elapsed_time}")


BUBBLE SORT

start_time = time.time()

elapsed_time = time.time() - start_time

print(f"Time: {elapsed_time}")

"""
#csv file actions for searching

searching_file = open("searching.csv", "w", newline="")

writer = csv.writer(searching_file)

writer.writerow(["Data Points", "Linear", "Binary"])

for i in range(5):
    i = i + 1
    sorted_array = create_sorted_array(i)
    item_to_find = sorted_array[random.randint(0, len(sorted_array) - 1)]
    Intro = "The number to find is {}" 
    print(Intro.format(item_to_find))
    writer.writerow([i, linear_search(sorted_array, item_to_find), binary_search(sorted_array, item_to_find)])

searching_file.close()


#Searching time code
"""
#LINEAR SEARCH

start_time = time.time()

elapsed_time = time.time() - start_time

print(f"Time: {elapsed_time}")


#BINARY SEARCH

start_time = time.time()

elapsed_time = time.time() - start_time

print(f"Time: {elapsed_time}")
"""